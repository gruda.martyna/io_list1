public class Calculator {
    public static int adding (int a, int b){
        int result = a + b;
        return result;
    }
    public static int substract (int a, int b){
        int result = a - b;
        return result;
    }
    public static int multiply (int a, int b){
        int result = a * b;
        return result;
    }
    public static float divide (int a, int b){
        if(b == 0){
            throw new ArithmeticException();
        }else {
            float result = (float) a / (float) b;
            return result;
        }
    }
    public static int power (int a, int b){
        int result = (int) Math.pow(a,b);
        return result;
    }
}
